// node_modules
const async = require('async');

// custom_modules
const Camera = require(global.rootpath + 'custom_modules/camera');
const Light = require(global.rootpath + 'custom_modules/light');
const vision = require(global.rootpath + 'custom_modules/vision');



var Nest = function(nest_config)
{  
  this.id = nest_config.id;
  this.camera = new Camera(this, nest_config.camera);
  this.light = new Light(this, nest_config.light);
    
  this.last_count = 0;
  
  this.ready = false;
};


Nest.prototype.init = function(callback)
{
  global.output.log('Init Nest');

  async.series(
    [
      this.initCamera.bind(this),
      this.initLight.bind(this)
    ],
    function(err)
    {
      this.ready = true;      
      return callback(err);
    }
  );
};


Nest.prototype.initCamera = function(callback)
{
  global.output.log('call Camera init');
  
  this.camera.init(function(err){
    return callback(err);
  });
};



Nest.prototype.initLight = function(callback)
{
  global.output.log('call Light init');
  
  this.light.init(function(err){
    return callback(err);
  });
};



Nest.prototype.countEggs = function(callback)
{
  var me = this;
  
  this.shoot(function(err){
    
    if(err)
    {
      return callback(err);
    }
    
    // compter
    vision.countEggs('/var/www/tmp/pic.' + me.id + '.png', function(err, count){

      if(err)
      {
        return callback(err);
      }

      // retourne le nb d'oeufs
      return callback(null, count);
    });    
  });
};



Nest.prototype.shoot = function(callback)
{

  var me = this;
  
  me.light.turnON(function(err){
    
    if(err)
    {
      global.output.log('light.turnON error');
      global.output.log(err);
      return callback(err);
    }
    
    me.camera.shoot(function(err){
      
      if(err)
      {
        global.output.log('camera.shoot error');
        global.output.log(err);
        return callback(err);
      }
      
      me.light.turnOFF(function(err){
        
        if(err)
        {
          global.output.log('light.turnON error');
          global.output.log(err);
          return callback(err);
        }
        
        return callback(null);
        
      }); // light.turnOFF
            
    }); // camera.shoot
    
  }); // light.turnON
};




module.exports = Nest;