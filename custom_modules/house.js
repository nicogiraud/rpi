// node_modules
const async = require('async');

// custom_modules
const Nest = require(global.rootpath + 'custom_modules/nest');
const Sigfox = require(global.rootpath + 'custom_modules/sigfox');
const Charger = require(global.rootpath + 'custom_modules/charger');
const Sensors = require(global.rootpath + 'custom_modules/sensors');


// Constructor
var House = function(house_config)
{
  this.id = house_config.id;
  this.client_id = house_config.client_id;
  
  // on crée les nids
  this.nests = [];
  
  var nests = this.nests
  house_config.nests.forEach(function(nest_config){
    nests.push(new Nest(nest_config));
  });
  
  this.sigfox = new Sigfox();
  this.charger = new Charger();
  
  this.ready = false;
};


// init
House.prototype.init = function(callback)
{
  var me = this;
  
  global.output.log('Init House');
  
  async.series(
    [
      this.initNests.bind(this),
      this.initSigfox.bind(this),
      this.initCharger.bind(this)
    ],
    function(err)
    {
      global.output.log('House OK');
      
      me.ready = true;
      
      me.current_nest_id = 0;
  
      setInterval(me.checkNest.bind(me), 10 * 1000);
      setInterval(me.checkStats.bind(me), 10 * 60 * 1000);
      
      return callback(err);
    }
  );
};


House.prototype.checkStats = function()
{
  var me = this;
  
  me.charger.getStats(function(err, stats){
    
    if(err)
    {
      console.log(err);
      return;
    }
    
    me.sigfox.send('S', stats, function(err){
      
      if(err)
      {
        console.log(err);
        return;
      }
    });
    
  });
};



House.prototype.checkNest = function()
{
  var me = this;
  
  var nest = me.nests[me.current_nest_id];
  
  nest.countEggs(function(err, count){
  
    // nb d'oeuf a changé
    if(count !== nest.last_count)
    {
      // infos à envoyer
      var data = {
        nest_id : nest.id,
        count : count,
        last_chicken_id : Math.floor(Math.random(4))
      };
      
      // on envoie
      me.sigfox.send('E', data, function(err){
        
        if(err)
        {
          console.log(err);
        }
      });
    }
    
    // mise à jour du nb d'oeufs
    nest.last_count = count;
    
    // nid suivant
    me.current_nest_id++;
    if(me.current_nest_id >= me.nests.length)
    {
      me.current_nest_id = 0;
    }
    
  }) // countEggs
};



House.prototype.initNests = function(callback)
{
  global.output.log('call Nests init');
    
  async.eachOfSeries(
    
    // tous les nids
    this.nests,
    
    // à exécuter pour chaque nid
    function(nest, nest_id, callback)
    {
      // init du nid
      nest.init(function(err){
        
        return callback(err);
        
      });
    },
    
    // une fois tous les nids instanciés
    function(err)
    {      
      return callback(err);
    }    
  );  
}


House.prototype.initSigfox = function(callback)
{
  global.output.log('call Sigfox init');
 
  this.sigfox.init(function(err){
    
    return callback(err);
    
  })
}


House.prototype.initCharger = function(callback)
{
  global.output.log('call Charger init');
  
  this.charger.init(function(err){
    
    return callback(err);
    
  })
}


module.exports = House;