
var cv = require('opencv');
var fs = require('fs');

// custom modules
var Zones = require(global.rootpath + 'custom_modules/zones.js');


var vision = {};


// charge l'histogramme moyen
vision.histogram_avg = JSON.parse(fs.readFileSync('/var/www/app/histogram_avg.json'));


// --------------------------------------
// compte les oeufs dans l'image filename
// --------------------------------------

vision.countEggs = function(filename, callback)
{
  console.log('countEggs : ' + filename);
  
  var size = 18;
  
  cv.readImage(filename, function(err, im){
      
    if(err)
    {
      return callback(err);
    }
    
    // gray
    im.convertGrayscale();
    
     // resize
    im.resize(160,120);
    
    var lbp = vision.getLBP(im);
    
    lbp.save(filename +' .lbp.png');
    
    var w2 = lbp.width();
    var h2 = lbp.height();
    
    var zones = new Zones(size - 4);
    
    for(var x = 0; x<w2 - size; x++)
    {
      for(var y = 0; y<h2 - size; y++)
      {

        // fenetre d'observation
        var window = lbp.roi(x,y,size, size);

        var matches = 0;

        for(var i = 0; i < 2; i++)
        {
          for(var j = 0; j < 2; j++)
          {
            var w = window.roi(i*9, j*9,9,9);

            var h = vision.getHistogram(w);

            var distance = vision.distance(vision.histogram_avg[i*2 + j], h);

            // la distance est acceptable, on garde cette feature
            if(distance < 160)
            { 
              matches ++;
            }
            
          } // j
        } // /

        // on a assez de features pour ce point
        if(matches > 2)
        {
          zones.addPoint(x,y,matches);
        }

      } // y
    } // x
    
    im.release();
    lbp.release();
    
    return callback(null, zones.zones.length);
  }); // readimage
};



// ----------------
// crée une matrice
// ----------------

vision.createMatrix8U = function(h,w)
{  
  var m = new cv.Matrix(h,w,cv.Constants.CV_8UC1);
  
  var b = new Buffer(h*w);
  for(var i = 0; i< w*h; i++)
  {
    b[i] = 0;
  }
  
  m.put(b);
  
  return m;
}


// --------------------------
// calcule la LBP d'une image
// --------------------------

vision.getLBP = function(im)
{ 
  var w = im.width();
  var h = im.height();
  
  var lbp = this.createMatrix8U(h-2, w-2);
  
  for(var x = 1; x < w - 2; x++)
  {
    for(var y = 1; y < h - 2 ; y++)
    {
      var dx;
      var digit = 0;
      
      dx = im.pixel(y-1,x-1) - im.pixel(y,x);
      digit = digit + 128 * (dx>0?1:0);

      dx = im.pixel(y,x-1) - im.pixel(y,x);
      digit = digit + 64 * (dx>0?1:0);

      dx = im.pixel(y+1,x-1) - im.pixel(y,x);
      digit = digit + 32 * (dx>0?1:0);

      dx = im.pixel(y+1,x) - im.pixel(y,x);
      digit = digit + 16 * (dx>0?1:0);

      dx = im.pixel(y+1,x+1) - im.pixel(y,x);
      digit = digit + 8 * (dx>0?1:0);

      dx = im.pixel(y,x+1) - im.pixel(y,x);
      digit = digit + 4 * (dx>0?1:0);

      dx = im.pixel(y,x) - im.pixel(y,x);
      digit = digit + 2 * (dx>0?1:0);

      dx = im.pixel(y,x-1) - im.pixel(y,x);
      digit = digit + 1 * (dx>0?1:0);
      
      lbp.pixel(y, x, [digit]);
    }
  }
  return lbp;
};


// -----------------------------------
// Calcul de l'histogramme d'une image
// -----------------------------------

vision.getHistogram = function(m)
{ 
  var w = m.width();
  var h = m.height();
  
  var histogram = new Array(256);
  for(var i = 0; i < 256; i++)
  {
    histogram[i] = 0;    
  }
  
  for(var x = 1; x < w ; x++)
  {
    for(var y = 1; y < h ; y++)
    {
      histogram[m.pixel(y,x)]++;
    }
  }
  
  // on normalise à 255 (pour garder un max de résolution sur 8 bits)
  
  // nb de pixels de l'image
  var pixels = w * h;

  // normalisation à 255
  for(var i = 0; i < 256; i++)
  {
    histogram[i] = Math.floor(histogram[i] / pixels * 255);
  }
  
  return histogram;
}


// ---------------------------------------
// Distance ChiSquare entre 2 histogrammes
// ---------------------------------------

vision.distance = function(hist_a, hist_b)
{
  var distance = 0;
  
  for(var i=0; i<256; i++)
  {
    var s = hist_a[i] + hist_b[i];
    
    if(s)
    {
      var d = (hist_a[i] - hist_b[i]);
      d = d*d;
      
      distance += (d / s);
    }
  }
  
  return Math.floor(distance);
};

module.exports = vision;
