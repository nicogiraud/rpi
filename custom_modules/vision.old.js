// core modules
var fs = require('fs');

// node_modules
PNG = require('pngjs').PNG;

// custom modules
var Zones = require(global.rootpath + 'custom_modules/zones.js');


var vision = {};

// charge l'histogramme moyen
vision.histogram_avg = JSON.parse(fs.readFileSync('/var/www/app/histogram_avg.json'));


vision.countEggs = function(buffer, callback)
{
  
  console.log('countEggs');
  
  var image = new vision.Image();
  
  image.setFileBuffer(buffer, function(err){
    
    if(err)
    {
      return callback(err);
    }
    
    // dérivée LBP
    var lbp = vision.getLBP(image);
    
    // taille de la fenetre
    var size = 18;
    var s2 = size / 2;
    
    // gestion des zones
    var zones = new Zones(size - 4);
    
    // scane la lbp
    for(var x = 0; x < lbp.w - size; x++)
    {
      for(var y =0; y< lbp.h - size; y++)
      {
        // nb de cases correspondantes
        var matches = 0;
        
        // histogramme sur chacune des 4 cases
        for(var i = 0; i < 2; i++)
        {
          for(var j = 0; j < 2; j++)
          {
            // histogramme de cette zone
            var h = lbp.getHistogram(x + (i*s2), y + (j*s2), s2, s2);
            
            // distance entre cette zone et la moyenne
            var distance = vision.distance(vision.histogram_avg[i*2 + j], h);
            
            console.log(distance);
            
            // la distance est acceptable, on garde cette feature
            if(distance < 160)
            { 
              matches ++;
            }
          }
        }
        
        // on a assez de features pour ce point
        if(matches > 2)
        {
          zones.addPoint(x,y,matches);
        }
        
      } // for y
    } // for x
    
    return callback(null, zones.zones.length);
  })
};


vision.getLBP = function(image)
{
  console.log('getLBP');
  
  var lbp = new vision.Image(image.w-2, image.h-2);
  
  for(var x = 1; x < image.w - 2; x++)
  {
    for(var y = 1; y < image.h - 2; y++)
    {
      var p = image.getPixel(x,y);
      
      var dx;
      var digit = 0;
      
      dx = image.getPixel(x-1,y-1) - p;
      digit = digit + 128 * (dx>0?1:0);

      dx = image.getPixel(x,y-1) - p;
      digit = digit + 64 * (dx>0?1:0);

      dx = image.getPixel(x+1,y-1) - p;
      digit = digit + 32 * (dx>0?1:0);

      dx = image.getPixel(x+1,y) - p;
      digit = digit + 16 * (dx>0?1:0);

      dx = image.getPixel(x+1,y+1) - p;
      digit = digit + 8 * (dx>0?1:0);

      dx = image.getPixel(x,y+1) - p;
      digit = digit + 4 * (dx>0?1:0);

      dx = image.getPixel(x-1,y+1) - p;
      digit = digit + 2 * (dx>0?1:0);

      dx = image.getPixel(x-1,y) - p;
      digit = digit + 1 * (dx>0?1:0);
      
      lbp.setPixel(x-1, y-1, digit);
    }
  }
  
  return lbp;
}


vision.distance = function(hist_a, hist_b)
{
  var distance = 0;
  
  for(var i=0; i<256; i++)
  {
    var s = hist_a[i] + hist_b[i];
    
    if(s)
    {
      var d = (hist_a[i] - hist_b[i]);
      d = d*d;
      
      distance += (d / s);
    }
  }
  
  return Math.floor(distance);
}


vision.Image = function(w,h)
{
  if(w && h)
  {
    this.w = w;
    this.h = h;
    this.png = new PNG({width: w, height: h});
  }
  else
  {
    this.png = null;
  }
}


vision.Image.prototype.getFileBuffer = function()
{
  return PNG.sync.write(this.png);  
};


vision.Image.prototype.setFileBuffer = function(buffer, callback)
{
  this.png = PNG.sync.read(buffer);
  
  this.w = this.png.width;
  this.h = this.png.height;
  
  return callback(null);
};


vision.Image.prototype.getPixel = function(x, y)
{
  var id = (y * this.w + x) * 4;
  return this.png.data[id];
};

vision.Image.prototype.setPixel = function(x, y, v)
{
  var id = (y * this.w + x) * 4;
  this.png.data[id] = v;
  this.png.data[id+1] = v;
  this.png.data[id+2] = v;
  this.png.data[id+3] = 255;
};

vision.Image.prototype.getHistogram = function(left,top,width,height)
{
  // tableau de 256 cases
  var histogram = new Array(255);
  
  // tout à 0
  for(var i = 0; i< 256; i++)
  {
    histogram[i] = 0;
  }
  
  // compte les occurences de chaque couleur
  for(var x = left; x < left + width; x++)
  {
    for(var y = top; y < top + height; y++)
    {
      var c = this.getPixel(x,y);
      
      histogram[c]++;
    }
  }
  
  // nb de pixels de l'image
  var pixels = width * height;

  // normalisation à 255
  for(var i = 0; i < 256; i++)
  {
    histogram[i] = Math.floor(histogram[i] / pixels * 255);
  }
  
  // ok
  return histogram;
}

module.exports = vision;