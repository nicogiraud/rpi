// core modules
const exec = require('child_process').exec;
const fs = require('fs');


var Camera = function(nest, camera_config)
{
  this.nest = nest;
  
  // id systeme
  this.id = camera_config.id;
  this.device = camera_config.device;
  this.r = camera_config.r;
  this.skip = camera_config.skip;
};


Camera.prototype.init = function(callback)
{
  global.output.log('Init Camera in nest ' + this.nest.id);
   
  return callback(null);
};


// take a pic  : callback(err, buffer)
Camera.prototype.shoot = function(callback)
{
  global.output.log('Camera Shoot in nest ' + this.nest.id);
  
  var filename = '/var/www/tmp/pic.' + this.nest.id + '.png';
   
  var cmd = 'fswebcam --no-banner --device ' + this.device  + ' --png 0 --skip ' + this.skip + ' --resolution ' + this.r.w + 'x' + this.r.h + ' --greyscale --save ' + filename;
  
  exec(cmd, function(err, stdout, stderr) {
    
    if(err)
    {
      return callback(err);
    }
    
    return callback(null);
      
  }); // cmd 1 (fswebcam)
}

module.exports = Camera;