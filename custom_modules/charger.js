var Charger = function()
{
  
};


Charger.prototype.init = function(callback)
{
  global.output.log('Charger Init');
  
  return callback(null);
};

// tension délivrée par la batterie
Charger.prototype.getStats = function(callback)
{
  var stats = {
    sol_current : 2, 
    sol_voltage : 10,
    bat_current : 3,
    bat_voltage : 12,
    charger_temp : 20
  }
  
  return callback(null, stats);
};


module.exports = Charger;