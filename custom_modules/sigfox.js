var SerialPort = require('serialport');


var Sigfox = function(){
  this.port = null;
};


Sigfox.prototype.init = function(callback)
{
  global.output.log('Init Sigfox');
  
  this.port = new SerialPort('/dev/ttyAMA0');
  
  this.port.on('open', function() {
    return callback(null);
  });
  
  this.port.on('error', function(err) {
    return callback(err);
  });
};


// egg : E
// nest_id, egg_count, last_chicken_id
// stats : S
// sol_current (x10), sol_voltage(x10), bat_current (x10), bat_voltage (x10), charger_temp (x10)

    
Sigfox.prototype.send = function(data_type, data, callback)
{
  var payload = new Buffer(12);
  
  for(var i =0; i<12; i++)
  {
    payload[i] = 0;
  }
  
  if(data_type == 'E')
  {
    payload[0] = 69;  
    payload[1] = data.nest_id;
    payload[2] = data.count;
    payload[3] = data.last_chicken_id;
  }
  
  if(data_type == 'S')
  {
    payload[0] = 83;
    payload[1] = Math.floor(data.sol_current * 10);
    payload[2] = Math.floor(data.sol_voltage * 10);
    payload[3] = Math.floor(data.bat_current * 10);
    payload[4] = Math.floor(data.bat_voltage * 10);
    payload[5] = Math.floor(data.charger_temp * 5);
  }
  
  var message = '';
  
  for(var i = 0; i < 12; i++)
  {
    var v = payload[i].toString(16);
    
    if(payload[i]<15)
    {
      v = '0' + v;
    }
    
    message = message + v + ' ';
  }

  message = message.trim();
  
  var cmd = "AT$SS=" + message  + "\n";
  
  console.log(cmd);
  
  this.port.write(cmd, function(err) {
    
    if (err)
    {
      return callback(err);
    }
    
    console.log('message written');
    return callback(null);
  });
  
  
  
};

module.exports = Sigfox;
