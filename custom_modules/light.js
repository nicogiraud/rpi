// core modules
const exec = require('child_process').exec;


var Light = function(nest, light_config){

  this.nest = nest;
  
  // GPIO du raspi
  this.anode = light_config.anode;
  this.cathode = light_config.cathode;
  
};


Light.prototype.init = function(callback)
{
  global.output.log('Init Light in nest ' + this.nest.id);
  
  var cmd = 'gpio mode ' + this.anode + ' out; gpio mode ' + this.cathode + ' out; gpio write ' + this.anode + ' 0; gpio write ' + this.cathode + ' 0';

  exec(cmd, function(err, stdout, stderr) {
    if(err)
    {
      return callback(new Error('light.init Error'));
    }
    return callback(null);
  });
};


Light.prototype.turnON = function(callback)
{
  global.output.log('Light ON in nest ' + this.nest.id);
  
  var cmd = 'gpio write ' + this.anode + ' 1; gpio write ' + this.cathode + ' 0';

  exec(cmd, function(err, stdout, stderr) {
    if(err)
    {
      return callback(new Error('light.tunrON Error'));
    }
    return callback(null);
  });
};


Light.prototype.turnOFF = function(callback)
{
  global.output.log('Light OFF in nest ' + this.nest.id);
  
  var cmd = 'gpio write ' + this.anode + ' 0; gpio write ' + this.cathode + ' 0';

  exec(cmd, function(err, stdout, stderr) {
    if(err)
    {
      return callback(new Error('light.turnOFF Error'));
    }
    return callback(null);
  });
};

module.exports = Light;