// code modules
const EventEmitter = require('events').EventEmitter;
const util = require('util');
    
var RFIDSensor = function(nest, rfidsensor_config)
{
  
  this.nest = nest;
  
  this.id = rfidsensor_config.id;
    
  // dernière lecture
  this.last_read = null;
}


util.inherits(RFIDSensor, EventEmitter);

// init
RFIDSensor.prototype.init = function(callback)
{
  global.output.log('Init RFID Sensor');
    
  // ok
  return callback(null);
}


// Une poule est-elle présente
RFIDSensor.prototype.read = function()
{
  var read = null;
    
  if(this.last_read === null && read)
  {
    this.emit('chickenEnter', read);
  }

  if(this.last_read && read === null)
  {
    this.emit('chickenLeave', this.last_read);
  }
  
  if(this.last_read && read && this.last_read !== read)
  {
    this.emit('chickenChange', this.last_read, read);
  }
  
  if(this.last_read && read && this.last_read == read)
  {
    this.emit('chickenStay', read);
  }

  this.last_read = read;
}; 



module.exports = RFIDSensor;