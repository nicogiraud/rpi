var zones = function(max_distance)
{
  // distance max entre deux points dans la même zone
  this._max_distance = max_distance;
  
  this.zones = [];
}

zones.prototype.addPoint = function(x, y, i)
{
  if(this.zones.length === 0)
  {
    var z = new Zone();
    z.addPoint(x,y,i);
    
    this.zones.push(z);
    
    return;
  }

  // carré de la distance min
  var max_distance2 = Math.pow(this._max_distance,2);
  
  // zone retenue
  var selected_zone = null;
  
  // carré de la distance avec la zone retenue
  var selected_distance = null;
  
  
  this.zones.forEach(function(zone){
 
    // carré de la distance entre le nouveau point et cete zone
    var dist2 = Math.pow(Math.abs(x-zone.x),2) + Math.pow(Math.abs(y-zone.y),2);
    
    // ce point pourrait aller dans cette zone
    if(dist2 < max_distance2)
    {
      // pas encore de zone éligible
      if(selected_zone === null)
      {
        selected_zone = zone;
        selected_distance = dist2;
      }
      else
      {
        // déjà une zone retenue. On change si on est plus près
        if(dist2 < selected_distance)
        {
          selected_zone = zone;
          selected_distance = dist2;
        }
      }
    }
  });
  
  // une zone a été trouvée pour ce point
  if(selected_zone)
  {
    selected_zone.addPoint(x,y,i);
  }
  else
  {
    var z = new Zone();
    z.addPoint(x,y,i);
    this.zones.push(z);
  }
};

// une zone
var Zone = function()
{
  this.points = [];
  
  // position du centre de gravité
  this.x = 0;
  this.y = 0;
  
  // intensité de la zone
  this.i = 0;
};


Zone.prototype.addPoint = function(x,y,i)
{
  // ajoute le point
  this.points.push({x:x,y:y,i:i});
  
  // update du centre de gravité
  this.update();
}

// mise à jour du centre de gravité
Zone.prototype.update = function()
{
  var sum_x = 0;
  var sum_y = 0;
  var sum_i = 0;
  
  this.points.forEach(function(point){
    sum_x += point.i * point.x;
    sum_y += point.i * point.y;
    sum_i += point.i;
  });
  
  this.x = Math.floor(sum_x / sum_i);
  this.y = Math.floor(sum_y / sum_i);
  this.i = sum_i;
}

module.exports = zones;