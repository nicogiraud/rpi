var fs = require('fs');
const exec = require('child_process').exec;

// absolute path
global.rootpath = '/var/www/app/';

global.output = require(global.rootpath + 'custom_modules/output');

const Camera = require(global.rootpath + 'custom_modules/camera');
const Light = require(global.rootpath + 'custom_modules/light');

// load config
global.config = require('./config');

var c = new Camera(global.config.house.nests[0].camera);
var l = new Light(global.config.house.nests[0].light);

var path = '/var/www/app/collection/';
var num = '4';

c.init(function(err){
  
  l.init(function(err){
    
    l.turnON(function(err){
      
      var files = fs.readdirSync(path);
      var len = files.length;
      var filename = path + num + '.' + len + '.png'
      
      var cmd = 'fswebcam --no-banner --device ' + c.device  + ' --png 0 --skip ' + c.skip + ' --resolution ' + c.r.w + 'x' + c.r.h + ' --greyscale --save '  + filename;
  
      exec(cmd, function(err, stdout, stderr) {
       
        l.turnOFF(function(err){
                    
        }); // l.turnOFF
      }); // c.shoot
    }); // l.turnON
  }); // l.init
}); // c.init

