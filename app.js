// absolute path
global.rootpath = '/var/www/app/';

// sortie
global.output = require(global.rootpath + 'custom_modules/output');

// custom_modules
const House = require(global.rootpath + 'custom_modules/house');


// node_modules



// load config
global.config = require('./config');





// le poulailler
var house = new House(global.config.house);

// init du poulailler
house.init(function(err){
  
  if(err)
  {
    global.output.log('House.init ERR : ' + err.message);
  }
  
});