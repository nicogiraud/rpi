var SerialPort = require('serialport');

/*
SerialPort.list(function (err, ports) {
  ports.forEach(function(port) {
    console.log(port);
  });
});
*/

var port = new SerialPort('/dev/ttyAMA0');

port.on('open', function() {
  
  console.log('open');
  
  port.write("AT$SS=00 01 02 03 04 05 06 07\n", function(err) {
    if (err) {
      return console.log('Error on write: ', err.message);
    }
    console.log('message written');
    return;
  });
  
});

port.on('error', function(err) {
  console.log('Error: ', err.message);
})

port.on('data', function (data) {
  console.log('Data: ' + data);
});