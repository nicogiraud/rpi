module.exports = {
  
  house : 
  {
    id : '001',
    client_id : '001',
    
    nests : [

      // nest 0
      {
        id : 0,
        camera : 
        {
          id : 0,
          device : '/dev/video0',
          r : {
            w : 640,
            h : 480
          },
          skip : 10
        },
        rfidsensor : 
        {
          id : 0
        },
        light : 
        {
          anode : 27, // +
          cathode : 24 // -
        }
      }, // nest 0

      // nest 1
      {
        id : 1,
        camera : 
        {
          id : 1,
          device : '/dev/video1',
          r : {
            w : 640,
            h : 480
          },
          skip : 10
        },
        rfidsensor : 
        {
          id : 1
        },
        light : 
        {
          anode : 28, // +
          cathode : 25  // -
        }
      } // nest 1
      
    ] // nests
  } // house 
} // config